# Rozhden
Базовый проект для быстрой сборки LandingPage и более сложных проектов.
Базово уже подключен и инициализирован скрипт для обработки источников и utm-меток - Sourcebuster.js

Для его настройки необходимо перейти в head страницы и указать правильный хост, на котором будет размещен сайт (по умолчанию localhost).

Реализована всплывающая форма захвата на основе Fancybox 3 и уже настроена валидация базовых полей (Name, email, телефон).

# How to use

Для сборки и запуска проекта необходимы node.js и gulp + gulp-cli установленный глобально

- `npm run init` - скачиваем пакеты
- `npm run start` - запускаем сборку и сервер
- `npm run phpSubmit` - скачивает phpSubmit из репозитория

или через gulp

- ` npm i && gulp ` - Для установки
- ` gulp --production ` - Чтобы запустить сборку в режиме production
- ` FTP_PASS=password gulp build ` - deploy на сервер по ftp

---

В данную сборку включены и настроены следующие компоненты:

**CSS**

- Normalize.css - для сброса всех стилей

**JavaScript**

*  Sourcebuster - http://sbjs.rocks/sourcebuster/configure
*  Fancybox 3 - http://fancyapps.com/fancybox/3/
*  Single page nav - https://github.com/ChrisWojcik/single-page-nav
*  Jquery Validation Plugin - https://jqueryvalidation.org


---

**Update 17.11.2017**

- Добавлена поддержка PostCSS
- Оптимизирован gulpfile.js
- Добавлен оптимизированный .htaccess


**Update 08.11.2017**

- Добавлена поддержка es6 (Babel)
- Убрали bower, теперь качаем все из npm
- Исправлены некоторые ошибки в скриптах
- Добавлена поддержка .pug
- Заменили less на sass


**Update 22.04.2017**

- Подключен gulp-newer
- Подключен gulp-plumber для лога ошибок
- Оптимизированы некоторые задачи


**Update 20.03.2017**

- Настроена базовая валидация в форме захвата
- Подключен gulp-svgo
- Подключен gulp-clean-css
