/**
 * jQuery validation default settings
 * https://jqueryvalidation.org/jQuery.validator.setDefaults/
 */

/*
Валидация
*/
$(document).ready(function() {
    $('body').on('click', '.open-popup', function () {
        $($(this).attr('data-popup')).fadeIn('300');
    });
    $('form').each(function() {
        $(this).validate({
            ignore: [],
            errorClass: "error",
            validClass: "success",
            errorElement: "div",
            wrapper: "span",
            rules: {
                name: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                phone: {
                    required: true,
                    phone: true
                },
                email: {
                    required: true,
                    email: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                card: {
                    required: true,
                    exactlengths: [7, 12]
                },
                personalAgreement: {
                    required: true
                }
            },
            messages: {
                phone: {
                    phone: "Укажите номер телефона"
                },
                email: {
                    email: "Введите правильный email"
                },
                personalAgreement: {
                    required: "Согласие на обработку персональных данных обязательно"
                },
                card: {
                    exactlengths: "Номер карты должен содержать 7 или 12 цифр"
                }
            }
        });
        jQuery.validator.addMethod("exactlengths", function(value, element, params) {
            return this.optional(element) || (value.length == params[0] || value.length == params[1]);
        });
        jQuery.validator.addMethod("phone", function(value, element) {
            return this.optional(element) || /\+7\(\d+\)\d{3}-\d{2}-\d{2}/.test(value);
        });
    });
});






/*
Маска
*/
$(document).ready(function() {
    $('#lpc-phonenumber').inputmask({ mask: '+7(999)999-99-99', showMaskOnHover: false });
});





/*
Анимация появления "waypoints"
*/
$('.appear').each(function() {

    var show = $(this);

    $(this).waypoint({
        handler: function() {
            show.addClass('show');
        },
        offset: 'bottom-in-view'
    });
})



$(function() {
    // Показываем модальное окно с формой захвата
    $('.showModal').click(function() {
        $.fancybox.open({
            src: "#popup_form",
            type: "inline"
        });
    });
});


/*
Открытыие меню
*/
jQuery(document).ready(function() {
    jQuery('.menu__burger').click(function(menu) {
        jQuery(this).toggleClass('active');
        jQuery('.menu').toggleClass('open');
        menu.preventDefault();
    });
});

/*
Аккардион
*/
$('.accordion__itemtitle').on('click', function() {
    if ($(this).parent().hasClass('open')) {
        $(this).parent().removeClass('open');
    } else {
        $('.accordion__item').removeClass('open');
        $(this).parent().addClass('open');
    };
});



/*
Попап
*/
$("body").on("click", ".close-popup", function() {
    $(".popup-window").fadeOut('300');
});


/*
показывать пароль
*/
$(function() {
    $(".show-pass").on("click", function() {
        if ($(this).hasClass("show")) {
            $(this).prev("input").attr("type", "password")
            $(this).removeClass("show")
        } else {
            $(this).prev("input").attr("type", "text")
            $(this).addClass("show")
        }
    });
});

/*
Swiper
*/
$(document).ready(function() {
    //initialize swiper when document ready
    var swiper = new Swiper('.swiper-container', {
        slidesPerView: 4,
        spaceBetween: 50,
        breakpoints: {
            1260: {
                slidesPerView: 3,
                spaceBetween: 50
            },
            940: {
                slidesPerView: 2,
                spaceBetween: 50
            },
            690: {
                slidesPerView: 1,
                spaceBetween: 50
            },
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
});


// Tabs
jQuery(document).ready(function() {
    $('ul.tabs__caption').on('click', 'li:not(.active)', function() {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
    });
    $('body').on('click', '.menu-btn', function() {
        $('.menu-btn').toggleClass('menu-btn_active');
        $('.mobile-menu').toggleClass('mobile-menu_active');
        $('.mobile-menu-helper').toggleClass('mobile-menu-helper_active');
    });
    $('body').on('click', '.mobile-menu-helper_active', function() {
        $('.menu-btn').toggleClass('menu-btn_active');
        $('.mobile-menu').toggleClass('mobile-menu_active');
        $('.mobile-menu-helper').toggleClass('mobile-menu-helper_active');
    });
});


// Home page animation
$(document).ready(function() {
    var bannerList = 3,
        bannerIndex = 1;
    $('.img-trs').attr('data-slide-num', '0');
    setInterval(function(homeanimation) {
        if (bannerIndex <= bannerList) {
            $('.img-trs').attr('data-slide-num', bannerIndex);
            bannerIndex++;
        } else {
            bannerIndex = 1;
        }
    }, 5000);
});


// Select 2 – стилизация селекта.
$(document).ready(function() {
    $('.js-select-style').select2();
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtYWluLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogalF1ZXJ5IHZhbGlkYXRpb24gZGVmYXVsdCBzZXR0aW5nc1xuICogaHR0cHM6Ly9qcXVlcnl2YWxpZGF0aW9uLm9yZy9qUXVlcnkudmFsaWRhdG9yLnNldERlZmF1bHRzL1xuICovXG5cbi8qXG7QktCw0LvQuNC00LDRhtC40Y9cbiovXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpIHtcbiAgICAkKCdib2R5Jykub24oJ2NsaWNrJywgJy5vcGVuLXBvcHVwJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAkKCQodGhpcykuYXR0cignZGF0YS1wb3B1cCcpKS5mYWRlSW4oJzMwMCcpO1xuICAgIH0pO1xuICAgICQoJ2Zvcm0nKS5lYWNoKGZ1bmN0aW9uKCkge1xuICAgICAgICAkKHRoaXMpLnZhbGlkYXRlKHtcbiAgICAgICAgICAgIGlnbm9yZTogW10sXG4gICAgICAgICAgICBlcnJvckNsYXNzOiBcImVycm9yXCIsXG4gICAgICAgICAgICB2YWxpZENsYXNzOiBcInN1Y2Nlc3NcIixcbiAgICAgICAgICAgIGVycm9yRWxlbWVudDogXCJkaXZcIixcbiAgICAgICAgICAgIHdyYXBwZXI6IFwic3BhblwiLFxuICAgICAgICAgICAgcnVsZXM6IHtcbiAgICAgICAgICAgICAgICBuYW1lOiB7XG4gICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICBub3JtYWxpemVyOiBmdW5jdGlvbih2YWx1ZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuICQudHJpbSh2YWx1ZSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHBob25lOiB7XG4gICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICBwaG9uZTogdHJ1ZVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgZW1haWw6IHtcbiAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgIGVtYWlsOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICBub3JtYWxpemVyOiBmdW5jdGlvbih2YWx1ZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuICQudHJpbSh2YWx1ZSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGNhcmQ6IHtcbiAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgIGV4YWN0bGVuZ3RoczogWzcsIDEyXVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgcGVyc29uYWxBZ3JlZW1lbnQ6IHtcbiAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IHRydWVcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgbWVzc2FnZXM6IHtcbiAgICAgICAgICAgICAgICBwaG9uZToge1xuICAgICAgICAgICAgICAgICAgICBwaG9uZTogXCLQo9C60LDQttC40YLQtSDQvdC+0LzQtdGAINGC0LXQu9C10YTQvtC90LBcIlxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgZW1haWw6IHtcbiAgICAgICAgICAgICAgICAgICAgZW1haWw6IFwi0JLQstC10LTQuNGC0LUg0L/RgNCw0LLQuNC70YzQvdGL0LkgZW1haWxcIlxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgcGVyc29uYWxBZ3JlZW1lbnQ6IHtcbiAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IFwi0KHQvtCz0LvQsNGB0LjQtSDQvdCwINC+0LHRgNCw0LHQvtGC0LrRgyDQv9C10YDRgdC+0L3QsNC70YzQvdGL0YUg0LTQsNC90L3Ri9GFINC+0LHRj9C30LDRgtC10LvRjNC90L5cIlxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgY2FyZDoge1xuICAgICAgICAgICAgICAgICAgICBleGFjdGxlbmd0aHM6IFwi0J3QvtC80LXRgCDQutCw0YDRgtGLINC00L7Qu9C20LXQvSDRgdC+0LTQtdGA0LbQsNGC0YwgNyDQuNC70LggMTIg0YbQuNGE0YBcIlxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICAgIGpRdWVyeS52YWxpZGF0b3IuYWRkTWV0aG9kKFwiZXhhY3RsZW5ndGhzXCIsIGZ1bmN0aW9uKHZhbHVlLCBlbGVtZW50LCBwYXJhbXMpIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLm9wdGlvbmFsKGVsZW1lbnQpIHx8ICh2YWx1ZS5sZW5ndGggPT0gcGFyYW1zWzBdIHx8IHZhbHVlLmxlbmd0aCA9PSBwYXJhbXNbMV0pO1xuICAgICAgICB9KTtcbiAgICAgICAgalF1ZXJ5LnZhbGlkYXRvci5hZGRNZXRob2QoXCJwaG9uZVwiLCBmdW5jdGlvbih2YWx1ZSwgZWxlbWVudCkge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMub3B0aW9uYWwoZWxlbWVudCkgfHwgL1xcKzdcXChcXGQrXFwpXFxkezN9LVxcZHsyfS1cXGR7Mn0vLnRlc3QodmFsdWUpO1xuICAgICAgICB9KTtcbiAgICB9KTtcbn0pO1xuXG5cblxuXG5cblxuLypcbtCc0LDRgdC60LBcbiovXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpIHtcbiAgICAkKCcjbHBjLXBob25lbnVtYmVyJykuaW5wdXRtYXNrKHsgbWFzazogJys3KDk5OSk5OTktOTktOTknLCBzaG93TWFza09uSG92ZXI6IGZhbHNlIH0pO1xufSk7XG5cblxuXG5cblxuLypcbtCQ0L3QuNC80LDRhtC40Y8g0L/QvtGP0LLQu9C10L3QuNGPIFwid2F5cG9pbnRzXCJcbiovXG4kKCcuYXBwZWFyJykuZWFjaChmdW5jdGlvbigpIHtcblxuICAgIHZhciBzaG93ID0gJCh0aGlzKTtcblxuICAgICQodGhpcykud2F5cG9pbnQoe1xuICAgICAgICBoYW5kbGVyOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHNob3cuYWRkQ2xhc3MoJ3Nob3cnKTtcbiAgICAgICAgfSxcbiAgICAgICAgb2Zmc2V0OiAnYm90dG9tLWluLXZpZXcnXG4gICAgfSk7XG59KVxuXG5cblxuJChmdW5jdGlvbigpIHtcbiAgICAvLyDQn9C+0LrQsNC30YvQstCw0LXQvCDQvNC+0LTQsNC70YzQvdC+0LUg0L7QutC90L4g0YEg0YTQvtGA0LzQvtC5INC30LDRhdCy0LDRgtCwXG4gICAgJCgnLnNob3dNb2RhbCcpLmNsaWNrKGZ1bmN0aW9uKCkge1xuICAgICAgICAkLmZhbmN5Ym94Lm9wZW4oe1xuICAgICAgICAgICAgc3JjOiBcIiNwb3B1cF9mb3JtXCIsXG4gICAgICAgICAgICB0eXBlOiBcImlubGluZVwiXG4gICAgICAgIH0pO1xuICAgIH0pO1xufSk7XG5cblxuLypcbtCe0YLQutGA0YvRgtGL0LjQtSDQvNC10L3RjlxuKi9cbmpRdWVyeShkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XG4gICAgalF1ZXJ5KCcubWVudV9fYnVyZ2VyJykuY2xpY2soZnVuY3Rpb24obWVudSkge1xuICAgICAgICBqUXVlcnkodGhpcykudG9nZ2xlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICBqUXVlcnkoJy5tZW51JykudG9nZ2xlQ2xhc3MoJ29wZW4nKTtcbiAgICAgICAgbWVudS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIH0pO1xufSk7XG5cbi8qXG7QkNC60LrQsNGA0LTQuNC+0L1cbiovXG4kKCcuYWNjb3JkaW9uX19pdGVtdGl0bGUnKS5vbignY2xpY2snLCBmdW5jdGlvbigpIHtcbiAgICBpZiAoJCh0aGlzKS5wYXJlbnQoKS5oYXNDbGFzcygnb3BlbicpKSB7XG4gICAgICAgICQodGhpcykucGFyZW50KCkucmVtb3ZlQ2xhc3MoJ29wZW4nKTtcbiAgICB9IGVsc2Uge1xuICAgICAgICAkKCcuYWNjb3JkaW9uX19pdGVtJykucmVtb3ZlQ2xhc3MoJ29wZW4nKTtcbiAgICAgICAgJCh0aGlzKS5wYXJlbnQoKS5hZGRDbGFzcygnb3BlbicpO1xuICAgIH07XG59KTtcblxuXG5cbi8qXG7Qn9C+0L/QsNC/XG4qL1xuJChcImJvZHlcIikub24oXCJjbGlja1wiLCBcIi5jbG9zZS1wb3B1cFwiLCBmdW5jdGlvbigpIHtcbiAgICAkKFwiLnBvcHVwLXdpbmRvd1wiKS5mYWRlT3V0KCczMDAnKTtcbn0pO1xuXG5cbi8qXG7Qv9C+0LrQsNC30YvQstCw0YLRjCDQv9Cw0YDQvtC70YxcbiovXG4kKGZ1bmN0aW9uKCkge1xuICAgICQoXCIuc2hvdy1wYXNzXCIpLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgIGlmICgkKHRoaXMpLmhhc0NsYXNzKFwic2hvd1wiKSkge1xuICAgICAgICAgICAgJCh0aGlzKS5wcmV2KFwiaW5wdXRcIikuYXR0cihcInR5cGVcIiwgXCJwYXNzd29yZFwiKVxuICAgICAgICAgICAgJCh0aGlzKS5yZW1vdmVDbGFzcyhcInNob3dcIilcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICQodGhpcykucHJldihcImlucHV0XCIpLmF0dHIoXCJ0eXBlXCIsIFwidGV4dFwiKVxuICAgICAgICAgICAgJCh0aGlzKS5hZGRDbGFzcyhcInNob3dcIilcbiAgICAgICAgfVxuICAgIH0pO1xufSk7XG5cbi8qXG5Td2lwZXJcbiovXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpIHtcbiAgICAvL2luaXRpYWxpemUgc3dpcGVyIHdoZW4gZG9jdW1lbnQgcmVhZHlcbiAgICB2YXIgc3dpcGVyID0gbmV3IFN3aXBlcignLnN3aXBlci1jb250YWluZXInLCB7XG4gICAgICAgIHNsaWRlc1BlclZpZXc6IDQsXG4gICAgICAgIHNwYWNlQmV0d2VlbjogNTAsXG4gICAgICAgIGJyZWFrcG9pbnRzOiB7XG4gICAgICAgICAgICAxMjYwOiB7XG4gICAgICAgICAgICAgICAgc2xpZGVzUGVyVmlldzogMyxcbiAgICAgICAgICAgICAgICBzcGFjZUJldHdlZW46IDUwXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgOTQwOiB7XG4gICAgICAgICAgICAgICAgc2xpZGVzUGVyVmlldzogMixcbiAgICAgICAgICAgICAgICBzcGFjZUJldHdlZW46IDUwXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgNjkwOiB7XG4gICAgICAgICAgICAgICAgc2xpZGVzUGVyVmlldzogMSxcbiAgICAgICAgICAgICAgICBzcGFjZUJldHdlZW46IDUwXG4gICAgICAgICAgICB9LFxuICAgICAgICB9LFxuICAgICAgICBuYXZpZ2F0aW9uOiB7XG4gICAgICAgICAgICBuZXh0RWw6ICcuc3dpcGVyLWJ1dHRvbi1uZXh0JyxcbiAgICAgICAgICAgIHByZXZFbDogJy5zd2lwZXItYnV0dG9uLXByZXYnLFxuICAgICAgICB9LFxuICAgIH0pO1xufSk7XG5cblxuLy8gVGFic1xualF1ZXJ5KGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpIHtcbiAgICAkKCd1bC50YWJzX19jYXB0aW9uJykub24oJ2NsaWNrJywgJ2xpOm5vdCguYWN0aXZlKScsIGZ1bmN0aW9uKCkge1xuICAgICAgICAkKHRoaXMpXG4gICAgICAgICAgICAuYWRkQ2xhc3MoJ2FjdGl2ZScpLnNpYmxpbmdzKCkucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpXG4gICAgICAgICAgICAuY2xvc2VzdCgnZGl2LnRhYnMnKS5maW5kKCdkaXYudGFic19fY29udGVudCcpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKS5lcSgkKHRoaXMpLmluZGV4KCkpLmFkZENsYXNzKCdhY3RpdmUnKTtcbiAgICB9KTtcbiAgICAkKCdib2R5Jykub24oJ2NsaWNrJywgJy5tZW51LWJ0bicsIGZ1bmN0aW9uKCkge1xuICAgICAgICAkKCcubWVudS1idG4nKS50b2dnbGVDbGFzcygnbWVudS1idG5fYWN0aXZlJyk7XG4gICAgICAgICQoJy5tb2JpbGUtbWVudScpLnRvZ2dsZUNsYXNzKCdtb2JpbGUtbWVudV9hY3RpdmUnKTtcbiAgICAgICAgJCgnLm1vYmlsZS1tZW51LWhlbHBlcicpLnRvZ2dsZUNsYXNzKCdtb2JpbGUtbWVudS1oZWxwZXJfYWN0aXZlJyk7XG4gICAgfSk7XG4gICAgJCgnYm9keScpLm9uKCdjbGljaycsICcubW9iaWxlLW1lbnUtaGVscGVyX2FjdGl2ZScsIGZ1bmN0aW9uKCkge1xuICAgICAgICAkKCcubWVudS1idG4nKS50b2dnbGVDbGFzcygnbWVudS1idG5fYWN0aXZlJyk7XG4gICAgICAgICQoJy5tb2JpbGUtbWVudScpLnRvZ2dsZUNsYXNzKCdtb2JpbGUtbWVudV9hY3RpdmUnKTtcbiAgICAgICAgJCgnLm1vYmlsZS1tZW51LWhlbHBlcicpLnRvZ2dsZUNsYXNzKCdtb2JpbGUtbWVudS1oZWxwZXJfYWN0aXZlJyk7XG4gICAgfSk7XG59KTtcblxuXG4vLyBIb21lIHBhZ2UgYW5pbWF0aW9uXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpIHtcbiAgICB2YXIgYmFubmVyTGlzdCA9IDMsXG4gICAgICAgIGJhbm5lckluZGV4ID0gMTtcbiAgICAkKCcuaW1nLXRycycpLmF0dHIoJ2RhdGEtc2xpZGUtbnVtJywgJzAnKTtcbiAgICBzZXRJbnRlcnZhbChmdW5jdGlvbihob21lYW5pbWF0aW9uKSB7XG4gICAgICAgIGlmIChiYW5uZXJJbmRleCA8PSBiYW5uZXJMaXN0KSB7XG4gICAgICAgICAgICAkKCcuaW1nLXRycycpLmF0dHIoJ2RhdGEtc2xpZGUtbnVtJywgYmFubmVySW5kZXgpO1xuICAgICAgICAgICAgYmFubmVySW5kZXgrKztcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGJhbm5lckluZGV4ID0gMTtcbiAgICAgICAgfVxuICAgIH0sIDUwMDApO1xufSk7XG5cblxuLy8gU2VsZWN0IDIg4oCTINGB0YLQuNC70LjQt9Cw0YbQuNGPINGB0LXQu9C10LrRgtCwLlxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XG4gICAgJCgnLmpzLXNlbGVjdC1zdHlsZScpLnNlbGVjdDIoKTtcbn0pOyJdLCJmaWxlIjoibWFpbi5qcyIsInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
