<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
$APP = new App();





define('CITY', $APP->getCity($_SERVER['REMOTE_ADDR']));
define('DATE', $APP->getDate());







class App {

    private static $CITIES = './php/cities.json';

    /**
     * @param string|bool $ip
     * @return object
     */
    function getIpInfo($ip = false)
    {
        $ip = (!$ip) ? $_SERVER['REMOTE_ADDR'] : $ip;
        $service = "http://ip-api.com/json/".$ip."?lang=ru";
        $info = "";
        if(function_exists('curl_version')) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $service);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_TIMEOUT, 1);
            $info = curl_exec($ch); //Получаем ответ
            curl_close($ch);
        } else {
            $info = file_get_contents($service);
        }
        $info = json_decode($info);
        return $info;
    }


    /**
     * @param bool $ip
     * @param string $p
     * @return string
     */
    function getCity($ip, $p = "p")
    {
        $ipinfo = $this->getIpInfo($ip);
        $u_city = $ipinfo->city;
        $cities = file_get_contents($this::$CITIES);
        $cities = json_decode($cities);
        $u_city = (isset($cities->$u_city->$p)) ? $cities->$u_city->$p : 'г. '.$u_city;
        return $u_city;
    }
    
    
    function getDate(){
	    $date = explode(".", date('j.m.Y', strtotime("+1 month")));
		switch ($date[1]){
		    case 1: $m='января'; break;
		    case 2: $m='февраля'; break;
		    case 3: $m='марта'; break;
		    case 4: $m='апреля'; break;
		    case 5: $m='мая'; break;
		    case 6: $m='июня'; break;
		    case 7: $m='июля'; break;
		    case 8: $m='августа'; break;
		    case 9: $m='сентября'; break;
		    case 10: $m='октября'; break;
		    case 11: $m='ноября'; break;
		    case 12: $m='декабря'; break;
		}
		$dateecho = $date[0]." ".$m;
		return $dateecho;
    }

}






