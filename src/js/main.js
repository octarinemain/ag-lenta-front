/**
 * jQuery validation default settings
 * https://jqueryvalidation.org/jQuery.validator.setDefaults/
 */

/*
Валидация
*/
$(document).ready(function() {
    $('body').on('click', '.open-popup', function () {
        $($(this).attr('data-popup')).fadeIn('300');
    });
    $('form').each(function() {
        $(this).validate({
            ignore: [],
            errorClass: "error",
            validClass: "success",
            errorElement: "div",
            wrapper: "span",
            rules: {
                name: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                phone: {
                    required: true,
                    phone: true
                },
                email: {
                    required: true,
                    email: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                card: {
                    required: true,
                    exactlengths: [7, 12]
                },
                personalAgreement: {
                    required: true
                }
            },
            messages: {
                phone: {
                    phone: "Укажите номер телефона"
                },
                email: {
                    email: "Введите правильный email"
                },
                personalAgreement: {
                    required: "Согласие на обработку персональных данных обязательно"
                },
                card: {
                    exactlengths: "Номер карты должен содержать 7 или 12 цифр"
                }
            }
        });
        jQuery.validator.addMethod("exactlengths", function(value, element, params) {
            return this.optional(element) || (value.length == params[0] || value.length == params[1]);
        });
        jQuery.validator.addMethod("phone", function(value, element) {
            return this.optional(element) || /\+7\(\d+\)\d{3}-\d{2}-\d{2}/.test(value);
        });
    });
});






/*
Маска
*/
$(document).ready(function() {
    $('#lpc-phonenumber').inputmask({ mask: '+7(999)999-99-99', showMaskOnHover: false });
});





/*
Анимация появления "waypoints"
*/
$('.appear').each(function() {

    var show = $(this);

    $(this).waypoint({
        handler: function() {
            show.addClass('show');
        },
        offset: 'bottom-in-view'
    });
})



$(function() {
    // Показываем модальное окно с формой захвата
    $('.showModal').click(function() {
        $.fancybox.open({
            src: "#popup_form",
            type: "inline"
        });
    });
});


/*
Открытыие меню
*/
jQuery(document).ready(function() {
    jQuery('.menu__burger').click(function(menu) {
        jQuery(this).toggleClass('active');
        jQuery('.menu').toggleClass('open');
        menu.preventDefault();
    });
});

/*
Аккардион
*/
$('.accordion__itemtitle').on('click', function() {
    if ($(this).parent().hasClass('open')) {
        $(this).parent().removeClass('open');
    } else {
        $('.accordion__item').removeClass('open');
        $(this).parent().addClass('open');
    };
});



/*
Попап
*/
$("body").on("click", ".close-popup", function() {
    $(".popup-window").fadeOut('300');
});


/*
показывать пароль
*/
$(function() {
    $(".show-pass").on("click", function() {
        if ($(this).hasClass("show")) {
            $(this).prev("input").attr("type", "password")
            $(this).removeClass("show")
        } else {
            $(this).prev("input").attr("type", "text")
            $(this).addClass("show")
        }
    });
});

/*
Swiper
*/
$(document).ready(function() {
    //initialize swiper when document ready
    var swiper = new Swiper('.swiper-container', {
        slidesPerView: 4,
        spaceBetween: 50,
        breakpoints: {
            1260: {
                slidesPerView: 3,
                spaceBetween: 50
            },
            940: {
                slidesPerView: 2,
                spaceBetween: 50
            },
            690: {
                slidesPerView: 1,
                spaceBetween: 50
            },
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
});


// Tabs
jQuery(document).ready(function() {
    $('ul.tabs__caption').on('click', 'li:not(.active)', function() {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
    });
    $('body').on('click', '.menu-btn', function() {
        $('.menu-btn').toggleClass('menu-btn_active');
        $('.mobile-menu').toggleClass('mobile-menu_active');
        $('.mobile-menu-helper').toggleClass('mobile-menu-helper_active');
    });
    $('body').on('click', '.mobile-menu-helper_active', function() {
        $('.menu-btn').toggleClass('menu-btn_active');
        $('.mobile-menu').toggleClass('mobile-menu_active');
        $('.mobile-menu-helper').toggleClass('mobile-menu-helper_active');
    });
});


// Home page animation
$(document).ready(function() {
    var bannerList = 3,
        bannerIndex = 1;
    $('.img-trs').attr('data-slide-num', '0');
    setInterval(function(homeanimation) {
        if (bannerIndex <= bannerList) {
            $('.img-trs').attr('data-slide-num', bannerIndex);
            bannerIndex++;
        } else {
            bannerIndex = 1;
        }
    }, 5000);
});


// Select 2 – стилизация селекта.
$(document).ready(function() {
    $('.js-select-style').select2();
});